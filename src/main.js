//console.log('Welcome to ', {title:'PizzaLand', emoji: '🍕'});

/*
console.warn("warning");
console.error("erreur");
console.clear();
*/
/*
let what = 'door';
console.log('Hold', 'the', what );
*/

//D)
/*
const name = "Regina";
const url = `images/${name.toLowerCase()}.jpg`;
let html = `<article class="pizzaThumbnail">
                <a href="${url}">
                    <img src="${url}"/>
                    <section>${name}</section>
                </a>
            <article>`;


console.log(url);
console.log(html);

document.querySelector('.pageContent').innerHTML = html;
*/

//E)

const dataBase = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];
/*
let html="";
for (let index = 0; index < data.length; index++) {
    const element = data[index];
    const url = `images/${element.toLowerCase()}.jpg`;
    html += `<article class="pizzaThumbnail">
                    <a href="${url}">
                        <img src="${url}"/>
                        <section>${element}</section>
                    </a>
                </article>`;
}
*/
const filtre1 = dataBase.filter(e => e.base == "tomate");

const filtre2 = dataBase.filter(e => e.price_small < 6);

const filtre3 = dataBase.filter(e => {
    let cpt = 0
    for (let index = 0; index < e.name.length; index++) {
        if(e.name.charAt(index) == 'i')
            cpt++
    }
    if (cpt == 2){
        return true;
    }
    
} );

const data = filtre3;

//G)
function compareAlpha(a, b) {
    let res = 0;
     if(a.name > b.name){
        res = 1;
    }else if(a.name < b.name){
        res = -1;
    }   
    return res;
}

function comparePrice(a, b) {
    let res = a.price_small - b.price_small;
    return res;
}
function comparePrice2(a, b) {
    let res = a.price_small - b.price_small;
    if (res == 0){
        res = a.price_large - b.price_large;
    }
    return res;
}



data.sort(compareAlpha);

const tab = [];
data.forEach(element => {

    tab.push(`<article class="pizzaThumbnail">
                <a href="${element.image}">
                    <img src="${element.image}"/>
                    <section>
                        <h4>${element.name}</h4>
                        <ul>
                            <li>Prix petit format : ${element.price_small}€</li>
                            <li>Prix grand format : ${element.price_large}€</li>
                        </ul>
                    </section>
                </a>
              </article>`)
});
let html = tab.join('');

document.querySelector('.pageContent').innerHTML = html;





  
